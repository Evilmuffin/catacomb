module Command_check
  
  def Command_check.valid_command(player_input)
    valid = ["l", "look", "north", "east", "south", "west", "up", "down", 
             "quit", "open door north", "open door east", "open door south",
             "open door west", "open door up", "open door down", 
             "unlock door north", "unlock door east", "unlock door south", 
             "unlock door west", "unlock door up", "unlock door down", 
             "kill john", "kill paul", "get key", "get stone", "inventory",
             "inv", "look plate", "kill white blob", "kill red blob",
             "kill green blob"]
    while valid.include?(player_input) == false
      invalid = ["Blah blah blah", 
          "My other car is a Lamborghini.",
          "I'm not listening to you anymore.",
          "If I want you to typo, I would've told you.",
          "Stop trying to type with yoour toes!",
          "Right!  And you are smart too!",
          "Maybe you should use a different keyboard...",
          "Hey!  I did a typo just like that!",
          "That was type #239098....Thanks for participating!",
          "You realiz that made absolutely no sense at all?"].sample
      puts invalid
      print "\n> "
      input_again = gets.chomp
      player_input = input_again.downcase
    end
    return player_input
  end
  
end

    
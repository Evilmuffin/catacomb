class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end
end

def get_input
  print "\n> "
  input = gets.chomp.downcase
  return input
end

def print_room(room_name, room_description, obj, mob, rexit)
  puts room_name.light_blue
  puts room_description
  if obj[0] != ""
    obj.each {|objs| puts "#{objs} is lying on the floor here.".green}
  end
  if mob[0] != ""
    mob.each {|mobs| puts "#{mobs} is standing here.".yellow}
  end
  puts rexit
end

def move(move_to, exit_to)
  if move_to == "north"
    room_to = "room" + exit_to[0]
  elsif move_to =="east"
    room_to = "room" + exit_to[1]
  elsif move_to == "south"
    room_to = "room" + exit_to[2]
  elsif move_to == "west"
    room_to = "room" + exit_to[3]
  elsif move_to == "up"
    room_to = "room" + exit_to[4]
  else
    room_to = "room" + exit_to[5]
  end
  return room_to
end

    
def run(room_num)
  
  initial = "initial" # Initializing input
  
  # Get initial room name, description, room exit, exit-to room, mobs, objs.
  room_name, room_description, rexit, valid_exit, invalid_exit, exit_to, mob, 
  obj, dexists, dlock = Rooms.send(room_num, initial)
  
  #print room
  print_room(room_name, room_description, obj, mob, rexit)
  
  input = get_input
  
  while valid_exit.include?(input) == false
    input = Command_check.valid_command(input) # check for valid command.
    
    if invalid_exit.include?(input) # input not = valid exit
      puts "Alas, you cannot go that way..."
      input = get_input
    elsif input == "l" || input == "look" # print room again if play type "look" or "l"
      print_room(room_name, room_description, obj, mob, rexit)
      input = get_input
    elsif input == "quit" # Player quit!
      puts "Thanks for playing!"
      puts "Good bye!"
      exit(0)
    elsif input == "inv" || input == "inventory"
      puts "You are carrying:"
      inventory = File.open("./inventory").read
      puts inventory.green
      input = get_input
    elsif valid_exit.include?(input) == true # Break out of the loop if input is valid exit
      break
    else # perform other action including unlock door, open door, kill mob, pick up items, etc.
      room_name, room_description, rexit, valid_exit, invalid_exit, exit_to, 
      mob, obj, dexists, dlock = Rooms.send(room_num, input)
      input = get_input
    end
  end

  # Move to another room.
  room_to = move(input, exit_to) 
  run(room_to)
  
end
    
  

require "./valid_command.rb"
require "./rooms.rb"

#initializing doors
door_status = File.open("./doors", 'w')
door_status.write("close\n")
door_status.write("close\n")
door_status.write("close")
door_status.close

#initializing mobs
mob_status = File.open("./mobs", 'w')
mob_status.write("notload\n")
mob_status.write("notload\n")
mob_status.write("notload")
mob_status.close

#initializing objs
obj_status = File.open("./objs", 'w')
obj_status.write("notload\n")
obj_status.write("notload")
obj_status.close

#initializing locks
lock_status = File.open("./locks", 'w')
lock_status.write("locked\n")
lock_status.write("locked")
lock_status.close

#initializing inventory
inv = File.open("./inventory", 'w')
inv.truncate(0)
inv.close

#initializing rooms solve
solve_status = File.open("./solved", 'w')
solve_status.write("solved\n")
solve_status.write("notsolved")
solve_status.close

#initializing room 1 solve
solve1_status = File.open("./room1solve", 'w')
solve1_status.write("false\n")
solve1_status.write("false\n")
solve1_status.write("false")
solve1_status.close

puts """
Basic commands:
Move:\t\tnorth, east, south, west, up, down
Look around:\tlook, l
Open door:\topen door 'direction'
Unlock door:\tunlock door 'direction'
Kill a mob:\tkill 'mob name'
Pick up item:\tget 'item name'
""".blue
puts "Hit Enter to continue."
gets.chomp

room_to = "roombegin"
run(room_to)

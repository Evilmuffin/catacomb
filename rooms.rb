module Rooms
  
  def Rooms.roombegin(input)
    
    room_name = "Lost in a dark forest".light_blue
    
    room_description = """
  You woke up in the middle of a dark forest.  There are trees all around you.
  Looking around you, there are nothing but trees.  Being completely lost, you
  see an big opening in one of the giant tree trunk.  You are thinking about
  going inside.  It will provide some kind of shetler anyways.
    """
    rexit = "Obvious Exits:\nEast\tInside the tree trunk"
    valid_exit = ["east"]
    invalid_exit = ["north", "south", "west", "up", "down"]
    exit_to = ["xx", "00", "xx", "xx", "xx", "xx"]
    dexists = [false, true, false, false, false, false]
    dlock = [false, false, false, false, false, false]
    
    mob = []
    obj = []
    
    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room00(input)
    
    room_name = "Inside the tree trunk".light_blue
    
    room_description = """
  You are now inside the tree trunk.  It is dark and damp here.  You also see
  a wooden staircase leading down to the unknown.
    """

    rexit = "Obvious Exits:\nWest\tLost in a dark forest\nDown\tWooden staircase"
    
    valid_exit = ["west", "down"]
    invalid_exit = ["north", "east", "south", "up"]
    
    exit_to = ["xx", "xx", "xx", "begin", "xx", "01"]
    
    mob = []
    
    obj = []
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room01(input)
    
    room_name = "Wooden staircase".light_blue
    
    room_description = """
  The wooden staircase zigzags between the entrance and deep underground.
    """

    rexit = "Obvious Exits:\nUp\tWooden staircase\nDown\tWooden staircase"
    
    valid_exit = ["up", "down"]
    invalid_exit = ["north", "east", "south", "west"]
    
    exit_to = ["xx", "xx", "xx", "xx", "00", "02"]
    
    mob = []
    
    obj = []
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room02(input)
    
    room_name = "Wooden staircase".light_blue
    
    room_description = """
  The wooden staircase zigzags between the entrance and deep underground.
    """

    rexit = "Obvious Exits:\nUp\tWooden staircase\nDown\tWooden staircase"
    
    valid_exit = ["up", "down"]
    invalid_exit = ["north", "east", "south", "west"]
    
    exit_to = ["xx", "xx", "xx", "xx", "01", "03"]
    
    mob = []
    
    obj = []
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room03(input)
    
    room_name = "Wooden staircase".light_blue
    
    room_description = """
  The wooden staircase zigzags between the entrance and deep underground.
  You see a door to your south.
    """
    
    door_status = File.readlines('./doors')
    if door_status[0].chomp == "close"
      rexit = "Obvious Exits:\nUp\tWooden staircase"
      valid_exit = ["up"]
      invalid_exit = ["north", "east", "south", "west", "up", "down"]
      exit_to = ["xx", "xx", "xx", "xx", "02", "xx"]
      dexists = [false, false, true, false, false, false]
    else
      rexit = "Obvious Exits:\nSouth\tInside the catacomb\nUp\tWooden staircase"
      valid_exit = ["south", "up"]
      invalid_exit = ["north", "east", "west", "down"]
      exit_to = ["xx", "xx", "04", "xx", "02", "xx"]
      dexists = [false, false, false, false, false, false]
    end
    
    mob = []
    
    obj = []
    
    if input == "open door south" && door_status[0].chomp == "close"
      rexit = "Obvious Exits:\nSouth\tInside the catacomb\nUp\tWooden staircase"
      valid_exit = ["south", "up"]
      invalid_exit = ["north", "east", "west", "down"]
      exit_to = ["xx", "xx", "04", "xx", "02", "xx"]
      dexists = [false, false, false, false, false, false]
      door_status[0] = "open\n"
      File.open('./doors', 'w') { |f| f.write(door_status.join) }
      puts "You open the door."
    elsif input == "open door south" && door_status[0].chomp == "open"
      puts "Show me how to open a door that is already opened, and I will show you a pig that flys."
    elsif input == "open door north" || input == "open door east" ||
          input == "open door west" || input == "open door up" || 
          input == "open door down"
      puts "You don't see a door in that direction."
    end
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room04(input)
    
    room_name = "Inside the catacomb".light_blue
    
    room_description = """
  You are inside the catacomb.  The ceiling is so high that you can't really
  see it in this darkness.  This stone crypt is lit by small lanterns
  hanging on the wall.  There are cells lining on the west side, and a
  small stream runs along the center bisecting it.
    """

    rexit = "Obvious Exits:\nNorth\tWooden staircase\nEast\tStone Steps\nSouth\tInside the catacomb"
    
    valid_exit = ["north", "east", "south"]
    invalid_exit = ["west", "up", "down"]
    
    exit_to = ["03", "14", "05", "xx", "xx", "xx"]
    
    mob = []
    
    obj = []
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room05(input)
    
    room_name = "Inside the catacomb 05".light_blue
    
    room_description = """
  You are inside the catacomb.  The ceiling is so high that you can't really
  see it in this darkness.  This stone crypt is lit by small lanterns
  hanging on the wall.  There are cells lining on the west side, and a
  small stream runs along the center bisecting it.
    """
    
    door_status = File.readlines('./doors')
    if door_status[1].chomp == "close"
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb"
      valid_exit = ["north", "south"]
      invalid_exit = ["east", "west", "up", "down"]
      exit_to = ["04", "xx", "06", "xx", "xx", "xx"]
      dexists = [false, false, false, true, false, false]
    else
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb\nWest\tInside a cell"
      valid_exit = ["north", "south", "west"]
      invalid_exit = ["east", "up", "down"]
      exit_to = ["04", "xx", "06", "50", "xx", "xx"]
      dexists = [false, false, false, false, false, false]
    end
    
    mob = []
    
    obj = []
    
    if input == "open door west" && door_status[1].chomp == "close"
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb\nWest\tInside a cell"
      valid_exit = ["north", "south", "west"]
      invalid_exit = ["east", "up", "down"]
      exit_to = ["04", "xx", "06", "50", "xx", "xx"]
      dexists = [false, false, false, false, false, false]
      door_status[0] = "open\n"
      File.open('./doors', 'w') { |f| f.write(door_status.join) }
      puts "You open the door."
    elsif input == "open door west" && door_status[1].chomp == "open"
      puts "Show me how to open a door that is already opened, and I will show you a pig that flys."
    elsif input == "open door north" || input == "open door east" ||
          input == "open door south" || input == "open door up" || 
          input == "open door down"
      puts "You don't see a door in that direction."
    end
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room06(input)
    
    room_name = "Inside the catacomb 06".light_blue
    
    room_description = """
  You are inside the catacomb.  The ceiling is so high that you can't really
  see it in this darkness.  This stone crypt is lit by small lanterns
  hanging on the wall.  There are cells lining on the west side, and a
  small stream runs along the center bisecting it.
    """

    rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb"
    
    valid_exit = ["north", "south"]
    invalid_exit = ["east", "west", "up", "down"]
    
    exit_to = ["05", "xx", "07", "xx", "xx", "xx"]
    
    mob = []
    
    obj = []
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room07(input)
    
    room_name = "Inside the catacomb 07".light_blue
    
    room_description = """
  You are inside the catacomb.  The ceiling is so high that you can't really
  see it in this darkness.  This stone crypt is lit by small lanterns
  hanging on the wall.  There are cells lining on the west side, and a
  small stream runs along the center bisecting it.
  
  There is a sign here on the wall:
  'Cell 1'
    """
    
    door_status = File.readlines('./doors')
    if door_status[2].chomp == "close"
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb"
      valid_exit = ["north", "south"]
      invalid_exit = ["east", "west", "up", "down"]
      exit_to = ["06", "xx", "08", "xx", "xx", "xx"]
      dexists = [false, false, false, true, false, false]
    else
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb\nWest\tInside a cell"
      valid_exit = ["north", "south", "west"]
      invalid_exit = ["east", "up", "down"]
      exit_to = ["06", "xx", "08", "51", "xx", "xx"]
      dexists = [false, false, false, false, false, false]
    end
    
    lock_status = File.readlines('./locks')
    dlock = [false, false, false, false, false, false]
    
    mob = []
    
    obj_status = File.readlines('./objs')
    obj = []
    
    if input == "open door west" && door_status[2].chomp == "close" && lock_status[0].chomp == "unlock"
      rexit = "Obvious Exits:\nNorth\tInside the catacomb\nSouth\tInside the catacomb\nWest\tInside a cell"
      valid_exit = ["north", "south", "west"]
      invalid_exit = ["east", "up", "down"]
      exit_to = ["04", "xx", "06", "51", "xx", "xx"]
      dexists = [true, false, true, true, false, false]
      door_status[2] = "open\n"
      File.open('./doors', 'w') { |f| f.write(door_status.join) }
      puts "You open the door."
    elsif input == "open door west" && door_status[2].chomp == "open"
      puts "Show me how to open a door that is already opened, and I will show you a pig that flys."
    elsif input == "open door west" && lock_status[0].chomp == "locked"
      puts "It is locked."
    elsif input == "open door north" || input == "open door east" ||
          input == "open door south" || input == "open door up" || 
          input == "open door down"
      puts "You don't see a door in that direction."
    end
    
    if input == "unlock door west" && lock_status[0].chomp == "locked" && obj_status[0].chomp == "inv" && door_status[2].chomp == "close" 
      puts "You unlock the door"
      lock_status[0] = "unlock\n"
      File.open('./locks', 'w') { |f| f.write(lock_status.join) }
    elsif input == "unlock door west" && lock_status[0].chomp == "unlock" && obj_status[0].chomp == "inv"
      puts "DUH! It is already unlocked!"
    elsif input == "unlock door west" && door_status[2].chomp == "open"
      puts "Hello?!  The door is already opened?"
    elsif input == "unlock door west" && obj_status[0].chomp != "inv"
      puts "You don't have the right key."
    elsif input == "unlock door north" || input == "unlock door east" ||
          input == "unlokc door south" || input == "unlock door up" || 
          input == "unlock door down"
      puts "What door?"
    end

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  
  
  
  
  
  def Rooms.room50(input)
    
    room_name = "Inside a cell".light_blue
    
    room_description = """
  You are now inside one of the cell.  It is surprising neat, just a
  layer of fine dust everywhere.  The cell is practically empty, with
  nothing, not even sign of someone who used to occupy it.
  
  There is a brass plate nailed to the wall here.
    """

    rexit = "Obvious Exits:\nEast\tInside the catacomb"
    
    valid_exit = ["east"]
    invalid_exit = ["north", "south", "west", "up", "down"]
    
    exit_to = ["xx", "05", "xx", "xx", "xx", "xx"]
    
    mob = []
    
    obj_status = File.readlines('./objs')
    if obj_status[0].chomp == "floor" 
      obj = ["The key of cell 1"]
    else
      obj = []
    end
    
    obj_status = File.readlines('./objs')
    if input == "look plate" && obj_status[0].chomp == "notload"
      puts "THE CRYPT\nSOLVE THE RIDDLES\nIN EACH CELL"
      puts "A key falls from the ceiling."
      obj_status[0] = "floor\n"
      File.open('./objs', 'w') { |f| f.write(obj_status.join) }
      obj = ["The key of cell 1"]
    elsif input == "look plate" && obj_status[0].chomp == "floor"
      puts "THE CRYPT\nSOLVE THE RIDDLES\nIN EACH CELL"
      obj = ["The key of cell 1"]
    elsif input == "look plate" && obj_status[0].chomp == "inv"
      puts "THE CRYPT\nSOLVE THE RIDDLES\nIN EACH CELL"
      obj = []
    end
    
    if input == "get key" && obj_status[0].chomp == "floor"
      puts "You get a key."
      obj_status[0] = "inv\n"
      File.open('./objs', 'w') { |f| f.write(obj_status.join) }
      File.open('./inventory', 'a+') { |f| f.write("A key of cell 1\n") }
      obj = []
    elsif input == "get key" && obj_status[0].chomp != "floor"
      puts "You don't see any key here."
    end
      
        
    
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
  def Rooms.room51(input)
    
    room_name = "Inside a cell".light_blue
    
    room_description = """
  You are now inside one of the cell.  It is surprising neat, just a
  layer of fine dust everywhere.  The cell is practically empty, with
  nothing, not even sign of someone who used to occupy it.
  
  There is a brass plate nailed to the wall here.
    """

    rexit = "Obvious Exits:\nEast\tInside the catacomb"
    
    valid_exit = ["east"]
    invalid_exit = ["north", "south", "west", "up", "down"]
    
    exit_to = ["xx", "07", "xx", "xx", "xx", "xx"]
    
    solve_status = File.readlines('./solved')
    solve1_status = File.readlines('./room1solve')
    mob_status = File.readlines('./mobs')
    if mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      mob = ["A white blob", "A red blob", "A green blob"]
    elsif mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "dead"
      mob = ["A white blob", "A red blob"]
    elsif mob_status[0].chomp == "alive" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "dead"
      mob = ["A white blob"]
    elsif mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      mob = ["A red blob", "A green blob"]
    elsif mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "dead"
      mob = ["A red blob"]
    elsif mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      mob = ["A red blob", "A green blob"]
    elsif mob_status[0].chomp == "dead" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "alive"
      mob = ["A green blob"]
    elsif mob_status[0].chomp == "dead" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "dead"
      mob = []
    elsif mob_status[0].chomp == "notload"
      mob = []
    end
    
    obj_status = File.readlines('./objs')
    if obj_status[1].chomp == "floor" 
      obj = ["The key of cell 2"]
    else
      obj = []
    end
    
    if input == "look plate" && obj_status[1].chomp == "notload" && solve_status[1].chomp == "notsolved"
      puts "CATHY STITICHED A BORDER\nGREEN, RED, AND WHITE\nIF ALL IS IN ORDER\nEVERYTHING'S ALL RIGHT"
      puts "A white blob falls from the ceiling.\nA red blob falls from the celling.\nA green blob falls from the celling"
      mob_status[0] = "alive\n"
      mob_status[1] = "alive\n"
      mob_status[2] = "alive"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      mob = ["A white blob", "A red blob", "A green blob"]
    elsif input == "look plate" && obj_status[1].chomp == "floor"
      puts "CATHY STITICHED A BORDER\nGREEN, RED, AND WHITE\nIF ALL IS IN ORDER\nEVERYTHING'S ALL RIGHT"
      obj = ["The key of cell 2"]
      mob = []
    elsif input == "look plate" && obj_status[1].chomp == "inv"
      puts "CATHY STITICHED A BORDER\nGREEN, RED, AND WHITE\nIF ALL IS IN ORDER\nEVERYTHING'S ALL RIGHT"
      obj = []
      mob = []
    elsif input == "look plate" && solve_status[1].chomp == "solved"
      puts "CATHY STITICHED A BORDER\nGREEN, RED, AND WHITE\nIF ALL IS IN ORDER\nEVERYTHING'S ALL RIGHT"
      obj = []
      mob = []
    end
    
    if input == "kill white blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      puts "You kill the white blob."
      mob_status[0] = "dead\n"
      solve1_status[0] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The red blob", "The green blob"]
    elsif input == "kill white blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "alive"
      puts "You kill the white blob."
      mob_status[0] = "dead\n"
      solve1_status[0] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The green blob"]
    elsif input == "kill white blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "dead"
      puts "You kill the white blob."
      mob_status[0] = "dead\n"
      solve1_status[0] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The red blob"]
    elsif input == "kill white blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "dead"
      puts "You kill the white blob."
      mob_status[0] = "dead\n"
      solve1_status[0] = "true\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = []
    elsif input == "kill red blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      puts "You kill the red blob."
      mob_status[1] = "dead\n"
      solve1_status[1] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The white blob", "The green blob"]
    elsif input == "kill red blob" && mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      puts "You kill the red blob."
      mob_status[1] = "dead\n"
      solve1_status[1] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The green blob"]
    elsif input == "kill red blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "dead"
      puts "You kill the red blob."
      mob_status[1] = "dead\n"
      solve1_status[1] = "true\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The white blob"]
    elsif input == "kill red blob" && mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "dead"
      puts "You kill the red blob."
      mob_status[1] = "dead\n"
      solve1_status[1] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = []
    elsif input == "kill green blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      puts "You kill the green blob."
      mob_status[2] = "dead"
      solve1_status[2] = "true\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The white blob", "The red blob"]
    elsif input == "kill green blob" && mob_status[0].chomp == "dead" && mob_status[1].chomp == "alive" && mob_status[2].chomp == "alive"
      puts "You kill the green blob."
      mob_status[2] = "dead"
      solve1_status[2] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The red blob"]
    elsif input == "kill green blob" && mob_status[0].chomp == "alive" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "alive"
      puts "You kill the green blob."
      mob_status[2] = "dead"
      solve1_status[2] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = ["The white blob"]
    elsif input == "kill green blob" && mob_status[0].chomp == "dead" && mob_status[1].chomp == "dead" && mob_status[2].chomp == "alive"
      puts "You kill the green blob."
      mob_status[2] = "dead"
      solve1_status[2] = "false\n"
      File.open('./mobs', 'w') { |f| f.write(mob_status.join) }
      File.open('./room1solve', 'w') { |f| f.write(solve1_status.join) }
      mob = []
    end
    
    solve1_status = File.readlines('./room1solve')
    if solve_status[1].chomp != "solved"
      if solve1_status[0].chomp == "true" && solve1_status[1].chomp == "true" && solve1_status[2].chomp == "true"
        solve_status[1] = "solved\n"
        File.open('./solved', 'w') { |f| f.write(solve_status.join) }
        puts "A key falls from the ceiling."
        obj_status[1] = "floor\n"
        File.open('./objs', 'w') { |f| f.write(obj_status.join) }
        obj = ["A key of cell 2"]
      end
    end
    
    obj_status = File.readlines('./objs')
    if input == "get key" && obj_status[1].chomp == "floor"
      puts "You get a key."
      obj_status[1] = "inv\n"
      File.open('./objs', 'w') { |f| f.write(obj_status.join) }
      File.open('./inventory', 'a+') { |f| f.write("A key of cell 2\n") }
      obj = []
    elsif input == "get key" && obj_status[1].chomp != "floor"
      puts "You don't see any key here."
    end
      
        
    
    
    dexists = [false, false, false, false, false, false]
    
    dlock = [false, false, false, false, false, false]

    return room_name, room_description, rexit, valid_exit, invalid_exit, exit_to,
           mob, obj, dexists, dlock
  end
  
end
